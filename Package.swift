// swift-tools-version:3.1

import PackageDescription

let package = Package(
    name: "VaporExtensions",
    targets: [
        Target(name: "VaporExtensions")
    ],
    dependencies: [
        .Package(url: "https://github.com/vapor/vapor.git", majorVersion: 2),
        .Package(url: "https://github.com/Quick/Nimble.git", majorVersion: 7)
    ],
    exclude: [
        "Config"
    ]
)
