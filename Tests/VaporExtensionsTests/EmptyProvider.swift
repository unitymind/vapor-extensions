import Vapor

public final class EmptyProvider: Vapor.Provider {
    public static let repositoryName = "provider"
    public init(config: Config) throws {}
    public func boot(_ config: Config) throws {}
    public func boot(_ droplet: Droplet) throws {}
    public func beforeRun(_ droplet: Droplet) throws {}
}