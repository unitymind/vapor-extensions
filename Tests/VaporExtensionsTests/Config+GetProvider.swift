import XCTest
import Nimble
import Vapor
@testable import VaporExtensions

class ConfigGetProvider: XCTestCase {
    func testPresent() throws {
        let config = try Config()
        try config.addProvider(EmptyProvider.self)
        let provider = config.getProvider(EmptyProvider.self)
        expect(provider).toNot(beNil())
        expect(provider).to(beAKindOf(EmptyProvider.self))
    }

    func testMissing() throws {
        let config = try Config()
        let provider = config.getProvider(EmptyProvider.self)
        expect(provider).to(beNil())
    }
}

extension ConfigGetProvider {
    static var allTests = [
        ("Missing", testMissing),
        ("Present", testPresent)
    ]
}
