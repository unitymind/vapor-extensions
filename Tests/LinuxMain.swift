import XCTest
@testable import VaporExtensionsTests

XCTMain([
    testCase(ConfigGetProvider.allTests)
])
