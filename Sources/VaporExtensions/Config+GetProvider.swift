import Vapor

extension Config {
    public func getProvider<P>(_ provider: P.Type) -> P? where P: Vapor.Provider {
        return providers.first(where: { type(of: $0) == P.self }) as? P
    }
}
